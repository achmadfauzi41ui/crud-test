package com.fauzi.crud.service;

import com.fauzi.crud.model.entity.User;
import com.fauzi.crud.model.response.GetUserByIdResponse;
import com.fauzi.crud.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GetUserByIdServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private GetUserByIdService getUserByIdService;

    private User user = User.builder()
            .name("testing")
            .id(1)
            .phone("098898212")
            .build();

    private GetUserByIdResponse expectedResponse = GetUserByIdResponse.builder()
            .id(1)
            .name("testing")
            .phone("098898212")
            .build();

    @Test
    public void getRetrieveBalanceSuccess(){
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));

        GetUserByIdResponse actualResponse = getUserByIdService.execute(1);

        Assertions.assertEquals(expectedResponse.getName(),actualResponse.getName());
        Assertions.assertEquals(expectedResponse.getPhone(),actualResponse.getPhone());
    }
}
