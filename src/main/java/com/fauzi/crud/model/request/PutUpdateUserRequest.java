package com.fauzi.crud.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PutUpdateUserRequest {

    private Integer id;

    private String name;
    private String phone;
}
