package com.fauzi.crud.controller;

import com.fauzi.crud.model.request.DelUserRequest;
import com.fauzi.crud.model.request.PostUserRequest;
import com.fauzi.crud.model.request.PutUpdateUserRequest;
import com.fauzi.crud.model.response.EmptyResponse;
import com.fauzi.crud.model.response.GetUserByIdResponse;
import com.fauzi.crud.model.response.PostUserResponse;
import com.fauzi.crud.service.DelUserService;
import com.fauzi.crud.service.GetUserByIdService;
import com.fauzi.crud.service.PostUserService;
import com.fauzi.crud.service.PutUpdateUserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/user")
public class UserController {
    private DelUserService delUserService;
    private GetUserByIdService getUserByIdService;
    private PostUserService postUserService;
    private PutUpdateUserService putUpdateUserService;

    public UserController(DelUserService delUserService,
                          GetUserByIdService getUserByIdService,
                          PostUserService postUserService,
                          PutUpdateUserService putUpdateUserService) {
        this.delUserService = delUserService;
        this.getUserByIdService = getUserByIdService;
        this.postUserService = postUserService;
        this.putUpdateUserService = putUpdateUserService;
    }

    @GetMapping(value = "/retrieve/{id}")
    public GetUserByIdResponse getUserById(@PathVariable Integer id){
        return getUserByIdService.execute(id);
    }

    @PostMapping(value = "/save")
    public PostUserResponse postUserSave(@RequestBody PostUserRequest request){
        return postUserService.execute(request);
    }

    @PutMapping(value = "/update")
    public EmptyResponse updateUserResponse(@RequestBody PutUpdateUserRequest request){
        return putUpdateUserService.execute(request);
    }

    @DeleteMapping(value = "/delete")
    public EmptyResponse deleteUserResponse(@RequestBody DelUserRequest request){
        return delUserService.execute(request);
    }

}
