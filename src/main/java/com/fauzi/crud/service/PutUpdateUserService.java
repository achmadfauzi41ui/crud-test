package com.fauzi.crud.service;

import com.fauzi.crud.exception.GeneralException;
import com.fauzi.crud.model.entity.User;
import com.fauzi.crud.model.request.PutUpdateUserRequest;
import com.fauzi.crud.model.response.EmptyResponse;
import com.fauzi.crud.repository.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PutUpdateUserService {
    private UserRepository userRepository;

    public PutUpdateUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public EmptyResponse execute(PutUpdateUserRequest request){
        User user = userRepository.findById(request.getId()).orElseThrow(()->{
            log.error("Can't find User By Id with request: {}", request.getId());
            throw new GeneralException(HttpStatus.CONFLICT, "4009","Can't find user by id request");
        });
        user.setName(request.getName());
        user.setPhone(request.getPhone());
        userRepository.save(user);
        return new EmptyResponse();
    }
}
