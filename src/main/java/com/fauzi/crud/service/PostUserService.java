package com.fauzi.crud.service;

import com.fauzi.crud.model.entity.User;
import com.fauzi.crud.model.request.PostUserRequest;
import com.fauzi.crud.model.response.PostUserResponse;
import com.fauzi.crud.repository.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PostUserService {
    private UserRepository userRepository;

    public PostUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public PostUserResponse execute(PostUserRequest request){
        User user = User.builder()
                .name(request.getName())
                .phone(request.getPhone())
                .build();

        userRepository.save(user);

        return PostUserResponse.builder()
                .id(user.getId())
                .name(user.getName())
                .phone(user.getPhone())
                .build();
    }
}
