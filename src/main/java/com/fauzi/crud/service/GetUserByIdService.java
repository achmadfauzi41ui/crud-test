package com.fauzi.crud.service;

import com.fauzi.crud.exception.GeneralException;
import com.fauzi.crud.model.entity.User;
import com.fauzi.crud.model.response.GetUserByIdResponse;
import com.fauzi.crud.repository.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class GetUserByIdService {
    private UserRepository userRepository;

    public GetUserByIdService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public GetUserByIdResponse execute(Integer id){
        User user = userRepository.findById(id).orElseThrow(()->{
            log.error("Can't find User By Id with request: {}", id);
            throw new GeneralException(HttpStatus.CONFLICT, "4009","Can't find user by id request");
        });
        return GetUserByIdResponse.builder()
                .id(user.getId())
                .name(user.getName())
                .phone(user.getPhone())
                .build();
    }
}
