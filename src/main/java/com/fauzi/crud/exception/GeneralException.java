package com.fauzi.crud.exception;

import org.springframework.http.HttpStatus;

public class GeneralException extends BaseException {

    public GeneralException(HttpStatus httpStatus, String errorCode, String errorDesc) {
        super(httpStatus, errorCode, errorDesc);
    }
    public GeneralException(String errorCode, String errorDesc){
        super(HttpStatus.BAD_REQUEST, errorCode, errorDesc);
    }
}

