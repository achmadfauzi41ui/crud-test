package com.fauzi.crud.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class BaseException extends RuntimeException {
    protected HttpStatus httpStatus;
    protected String errorCode;
    protected String errorDesc;

    public BaseException(HttpStatus httpStatus, String errorCode, String errorDesc) {
        this.httpStatus = httpStatus;
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
    }
}
